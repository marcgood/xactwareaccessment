var until          = protractor.ExpectedConditions;
var policyLink     = element(by.css('a[href=\'//www.jetblue.com/travel/baggage/\']'));
var txtBagContent  = element(by.css('#checked-baggage-content > h2')); 

module.exports = {

  viewPolicy: async function() {
    await browser.wait(until.visibilityOf(policyLink, 5000, 'Policy link not displayed.'));
    await policyLink.click();
  },

  checkBaggageContent: async function() {
    return await txtBagContent.getText();
  }

} // end module
