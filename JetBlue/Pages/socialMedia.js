var until       = protractor.ExpectedConditions;
//var socialTitle = browser.getTitle();
//var socialElement   = element(by.id('seo_h1_tag'));
//var socialElement   = element(by.id('channel-title')); 

var title, socialElement;

module.exports = {

    isSocialPageLoaded: async function( media ) {
        // Get right element off chosen social media page
        switch ( media ) {
            case  'facebook':
                socialElement = element(by.id('seo_h1_tag'));
                break;
            case 'twitter' :
                socialElement = element(by.css('a[class="ProfileHeaderCard-nameLink u-textInheritColor js-nav"]')); 
                break;
            case 'instagram' :
                socialElement = element(by.css('h1[title="jetblue"]')); 
                break;
            case 'youtube' :
                socialElement = element(by.id('channel-title')); 
                break;
        }

        await browser.getAllWindowHandles().then( function( handles ) {
            //switch to the last tab that isn't closed.
            browser.switchTo().window( handles[handles.length - 1] )
                .then( async function() { 
                    await browser.wait(until.visibilityOf(socialElement, 10000, 'The ' + media + ' social page was not loaded.'));
                    title = await browser.getTitle();
                });
        });

        return title;
    }
} // end module
