var until           = protractor.ExpectedConditions;
var txtUsername     = element(by.id('email_field'));
var txtPassword     = element(by.id('password_field'));
var btnSignIn       = element(by.id('signin_btn'));
var menuPlanATrip   = element(by.css('#jb-primary-links > li.first.submenu > a'));
var subMenuHotels   = element(by.css('#jb-primary-links > li.first.submenu > div > div > ul > li.hotels > a'));
var subMenuCars     = element(by.css('#jb-primary-links > li.first.submenu > div > div > ul > li.cars > a'));
var txtAirDepart    = element(by.id('jbBookerDepart'));
var txtAirArrive    = element(by.id('jbBookerArrive'));
var txtCalDepart   = element(by.id('jbBookerCalendarDepart'));
var txtCalReturn   = element(by.id('jbBookerCalendarReturn'));
var btnFindIt      = element(by.css('[ng-click="handleSubmit($event)"]'));
//var      = element(by.binding('username'));
var errorMsg       = element(by.repeater('errorMessage in errorMessages').row(0).column('errorMessage'));

module.exports = {

  signIn: async function( username, password ) {
    await browser.wait(until.visibilityOf(txtUsername, 10000, 'Username field not displayed.'));
    await browser.sleep(5000);
    await txtUsername.sendKeys(username);
    await txtPassword.sendKeys(password);
    await browser.sleep(5000);
    await btnSignIn.click();
  },

  planATrip: async function() {
    await browser.actions().mouseMove(menuPlanATrip).perform();
    await browser.wait(until.visibilityOf(subMenuCars, 4000, 'Cars icon did not show'));
    await subMenuCars.click();
  },

  checkFlights: async function( airDepart, airArrive, calDepart, calReturn ) {
    await txtAirDepart.clear();
    await txtAirDepart.sendKeys(airDepart);
    await txtAirArrive.sendKeys(airArrive);
    await txtCalDepart.sendKeys(calDepart);
    await txtCalReturn.sendKeys(calReturn);
    await btnFindIt.click();
  },

  checkFlightsBadReturnDate: async function(airDepart, airArrive, calDepart, badCalReturn) {
    await txtAirDepart.clear();
    await txtAirDepart.sendKeys(airDepart);
    await txtAirArrive.sendKeys(airArrive);
    await txtCalDepart.sendKeys(calDepart);
    await txtCalReturn.sendKeys(badCalReturn);
    await btnFindIt.click();
    return await errorMsg.getText();
  },

  closeTab: async function() {
  await browser.getAllWindowHandles().then(function (handles) {
      browser.driver.close();   // close current tab
      browser.switchTo().window(handles[0]);  // refocus back to original tab
  });
  },

  selectSocialMedia: async function( media ) {
    //await element(by.css('li[class="facebook"]')).click();
    element(by.css('#jb-footer > div.body-wrap.clearfix > div.social-wrap.clearfix > ul > li.'+ media +' > div > a')).click();
  }

} // end module
