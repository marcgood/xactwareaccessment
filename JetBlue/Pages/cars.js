var until       = protractor.ExpectedConditions;
//var lblMakeReservation = element(by.css('#trueblue > div > p'));
var lblMakeReservation = element(by.css('#pageBG > div > div.col-xs-12.col-sm-4.col-sm-pull-8.ncl-column > div > div > div > h3'));
var title;

module.exports = {

    checkCarsTitle: async function() {
        //var dfd = protractor.promise.defer();
        await browser.getAllWindowHandles().then( function( handles ) {
            //switch to the last tab that isn't closed.
            browser.switchTo().window( handles[handles.length - 1] )
                .then( async function() { 
                    await browser.wait(until.visibilityOf(lblMakeReservation, 10000, 'Make Reservation did not display.'));
                    title = await browser.getTitle();
                });
        });
        
        return title;
    }

} // end module