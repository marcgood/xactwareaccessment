var until           = protractor.ExpectedConditions;
var departFlights   = element(by.css('#AirFlightItinerarySelectForm > h3.flightSelectionSubTitle.flightSelectionSubTitleGT'));

module.exports = {

    isLoaded: async function() {
        await browser.wait(until.visibilityOf(departFlights), 10000, 'The book page did not load');
        return departFlights.isPresent();
      }

} // end module
