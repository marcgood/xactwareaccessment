var until                   = protractor.ExpectedConditions;
var welcomeTutorial         = element(by.className('step1'));
var welcomeToBadges         = element(by.css('body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltiptext > div > h3'));
var btnSkipTutorial         = element(by.css('[class*="introjs-skipbutton"]'));
var btnCancelAgreement      = element(by.css('[ng-click="closeThisDialog()"]'));
var joinBadges              = element(by.css('[ng-click="join()"]'));


module.exports = {

    isTutorialAndSkip: async function() {
        await browser.wait(until.visibilityOf(btnSkipTutorial, 5000, 'Badges page was not loaded.'));
        await btnSkipTutorial.click();
        await browser.sleep(500);  // Fixes race condition
        await btnCancelAgreement.click();
        
        return await joinBadges.isPresent();
    }

} // end module