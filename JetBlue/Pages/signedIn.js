// default home page
var until 	        = protractor.ExpectedConditions;
var txtDisplayName  = element(by.css('#main-inner > h3'));


module.exports = {
  
  getDisplayName: async function() {
    await browser.wait(until.visibilityOf(txtDisplayName, 10000, 'Display name not displayed.'));
    return await txtDisplayName.getText();
  },

  selectOptionBadges: async function() {
   await element(by.css('div[role="listbox"]')).click();
   await element(by.css('li[data-value*="badges.jetblue"]')).click();
  }

} // end module
