// Page Objects in use
var logIn     = require('../Pages/logIn.js');
var signedIn  = require('../Pages/signedIn.js'); 
var baggage	  = require('../Pages/baggage.js');
var badges    = require('../Pages/badges.js');
var cars      = require('../Pages/cars.js');
var book      = require('../Pages/book.js');
var social    = require('../Pages/socialMedia.js');

describe('Acceptance Tests', function() {
  var username        = 'uvuautoqa@gmail.com';
  var password        = 'J3tBlu33';
  var displayName     = 'Maximus';
  var lblCheckedBags  = 'Checked bags';
  var flyFrom         = 'Salt Lake City, UT (SLC)';
  var flyTo           = 'Seattle, WA (SEA)';
  var flyOutDate      = '02-21-2018'
  var flyReturnDate   = '02-24-2018';
  var socialMedia     = ['facebook', 'instagram', 'youtube']; // twitter removed, service being weird


  beforeEach( function() {
    browser.ignoreSynchronization = true;
    browser.get('https://www.jetblue.com');
  });

  // Test #1 - Title
  // Checkk the site's home page even loads
  it('should navigate to home page', async function() {
    
    expect(browser.getTitle()).toEqual('JetBlue | Airline Tickets, Flights, and Airfare');
  });

  // Test #2 - Login to account
  // Confirm login process works and loads account
    it('should sign in to JetBlue account', async function() {
    await logIn.signIn(username, password);

    expect(await signedIn.getDisplayName()).toContain(displayName);
  });


  // Test #3 - Access the Baggage page
  // Check the baggage info page loads
  it('should open the baggage info page', async function() {
    await baggage.viewPolicy(); // open policy page from home page
    var text = await baggage.checkBaggageContent();

    expect(text).toMatch(lblCheckedBags);
  });

  // Test #4 - Open Cars page from Plan A Trip animation
  // Make sure the menu animation works and redirects to new subdomain page
  it ('should open Cars page in new window', async function() {
    await logIn.planATrip();  
    browser.sleep(500);    
    var title = await cars.checkCarsTitle();
    await logIn.closeTab();

    expect('Welcome JetBlue Travelers').toContain(title);
  });

  // Test #5 - Confirm first page of book opens
  // After entering fly info, the next page should load
  // to keep searching for desired flight
  it('should display a flight between city A and city B', async function() {
    await logIn.checkFlights( flyFrom, flyTo, flyOutDate, flyReturnDate );
    
    expect(await book.isLoaded()).toBe(true);
  });

  // Test #6 - Confirm error message with bad date entry
  it('should display error with return date before the depart date', async function() {
    // Purposefully put return date before flyout date
    var badDateErrorMsg = await logIn.checkFlightsBadReturnDate( flyFrom, flyTo, flyReturnDate, flyOutDate );

    expect(badDateErrorMsg).toMatch('Please enter valid return date.');
  });
 
    // Test #7 - Should confirm social media links
    // Want links to social media to work so it can be followed
  it('should open each social media pages', async function() {
 
    function upperFirstLetter ( string ) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  
    for (var i = 0; i < socialMedia.length; ++i)  {
      media = socialMedia[i];

      await logIn.selectSocialMedia( media );
      await browser.sleep(500);  // prevent timeout by Jasmine
      var title = await social.isSocialPageLoaded( media );
      await logIn.closeTab();

      if (media == 'youtube')
        media = 'YouTube';
      else
        media = upperFirstLetter( media );

      expect(await title).toContain( media );
    }
  });
  
  // Test #8- Access the Badges page
  // Allow cust to check Badges page without committing
  it('should open the Badges page without joining', async function() {
    //var txtDisplayName  = element(by.css('#main-inner > h3'));
    ///await browser.sleep(1000);
    //if (!( await txtDisplayName.isPresent()))
    //wait logIn.signIn(username, password);
    await signedIn.selectOptionBadges();
    var badgesPageLoaded = await badges.isTutorialAndSkip();

    expect(badgesPageLoaded).toBe(true);
  });

// PSUEDO-CODE TERRITORY  
/*
  // Psuedo-code #1
  // Nobody likes a broken code integration
  it('should upload member photo', function() {
    // Access Account
    // Access TrueBlue Profile
    // Select Member Photo
    // Upload a valid size and file type
    // Expect upload as success
  });

  // Psuedo-code #2
  // It'd be silly to allow a 1TB file upload
  it('should not allow upload member photo > 4MB', function() {
    // Access Account
    // Access TrueBlue Profile
    // Select Member Photo
    // Attempt upload of > 4MB
    // Expect failed message as too large
  });
*/
}); // end describe